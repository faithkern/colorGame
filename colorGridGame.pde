//color flood game
//start on top left corner

int cr = 10; //cols and rows #
Square[][] matrix; //board

//5 colors:
int s1c1 = 226; int s1c2 = 58; int s1c3 = 49; //red
int s2c1 = 234; int s2c2 = 207; int s2c3 = 51; //yellow
int s3c1 = 49; int s3c2 = 188; int s3c3 = 72; //green
int s4c1 = 49; int s4c2 = 114; int s4c3 = 188; //blue
int s5c1 = 172; int s5c2 = 100; int s5c3 = 193; //purple
int a; //for picking color of each square randomly
int c1,c2,c3;

//check bounds of buttons:
int redrangeleft = 80;
int redrangeright = 204;
int yellowrangeleft = 264;
int yellowrangeright = 368;
int greenrangeleft = 448;
int greenrangeright = 552;
int bluerangeleft = 632;
int bluerangeright = 736;
int purplerangeleft = 816;
int purplerangeright = 920;
int y1 = 1025;
int y2 = 1175;

int moveCounter = 0; //to keep score

Button red;
Button yellow;
Button green;
Button blue;
Button purple;

//------------------------------------------------------------------ setup:

void setup(){
  size(1000,1350);
  
  matrix = new Square[cr][cr];
  
  //make starting board:
  for(int i = 0; i<cr; i++){
    for(int j = 0; j<cr; j++){
      a = int(random(5))+1;
      switch(a){
        case 1: c1 = s1c1; c2 = s1c2; c3 = s1c3; matrix[i][j] = new Square(i*100,j*100,c1,c2,c3,1); break;
        case 2: c1 = s2c1; c2 = s2c2; c3 = s2c3; matrix[i][j] = new Square(i*100,j*100,c1,c2,c3,2); break;
        case 3: c1 = s3c1; c2 = s3c2; c3 = s3c3; matrix[i][j] = new Square(i*100,j*100,c1,c2,c3,3); break;
        case 4: c1 = s4c1; c2 = s4c2; c3 = s4c3; matrix[i][j] = new Square(i*100,j*100,c1,c2,c3,4); break;
        case 5: c1 = s5c1; c2 = s5c2; c3 = s5c3; matrix[i][j] = new Square(i*100,j*100,c1,c2,c3,5); break;
      }
    }
  }
  
  red = new Button(80,1025,s1c1,s1c2,s1c3);
  yellow = new Button(264,1025,s2c1,s2c2,s2c3);
  green = new Button(448,1025,s3c1,s3c2,s3c3);
  blue = new Button(632,1025,s4c1,s4c2,s4c3);
  purple = new Button(816,1025,s5c1,s5c2,s5c3);
  
}

//------------------------------------------------------------------ draw:

void draw(){
  background(0);
  for(int i = 0; i<cr; i++){
    for(int j = 0; j<cr; j++){
      matrix[i][j].display();
    }
  }
  
  red.display();
  yellow.display();
  green.display();
  blue.display();
  purple.display();
  
  fill(255);
  textSize(100);
  String m = "Moves: "+moveCounter;
  text(m,80,1300);
  
}

//------------------------------------------------------------------ mouse clicked:

void mousePressed(){
  
  if(mouseX>redrangeleft && mouseX<redrangeright && mouseY>y1 && mouseY<y2){
    flood(1);
    moveCounter++;
  }
  else if(mouseX>yellowrangeleft && mouseX<yellowrangeright && mouseY>y1 && mouseY<y2){
    flood(2);
    moveCounter++;
  }
  else if(mouseX>greenrangeleft && mouseX<greenrangeright && mouseY>y1 && mouseY<y2){
    flood(3);
    moveCounter++;
  }
  else if(mouseX>bluerangeleft && mouseX<bluerangeright && mouseY>y1 && mouseY<y2){
    flood(4);
    moveCounter++;
  }
  else if(mouseX>purplerangeleft && mouseX<purplerangeright && mouseY>y1 && mouseY<y2){
    flood(5);
    moveCounter++;
  }
}

//------------------------------------------------------------------ flood:

void flood(int c){
  
  //recursively change colors
  Square s1 = matrix[0][0];
  int oldc = s1.getColor();
  
  //first change first square, then call the left and bottom ones recursively:
  s1.setColor(c);
  s1.setCheck(1);
  
  recurse(oldc,c,1,0);
  recurse(oldc,c,0,1);
  
  //now done, reset values to see if its been checked already:
  for(int i = 0; i<cr; i++){
    for(int j = 0; j<cr; j++){
      matrix[i][j].setCheck(0);
    }
  }
  
}

//------------------------------------------------------------------ recurse:

void recurse(int oldc, int newc, int x, int y){
  //for 1st square, if its color = old color, change to new color
  //then check around that one and call recurse
  
  if(matrix[x][y].getColor()==oldc){ //only if its the old color
    matrix[x][y].setColor(newc);
    
    //if not checked already:
    if(matrix[x][y].getCheck()==0){
      
      //cant be top left corner
      
      //else if top right corner, bottom and left:
      if((matrix[x][y].getY() == 0) && (matrix[x][y].getX() == 900)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x,y+1);
        recurse(oldc,newc,x-1,y);
      }
      //else if bottom left corner, right and top:
      else if((matrix[x][y].getY() == 900) && (matrix[x][y].getX() == 0)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x+1,y);
        recurse(oldc,newc,x,y-1);
      }
      //else if bottom right corner, top and left:
      else if((matrix[x][y].getY() == 900) && (matrix[x][y].getX() == 900)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x,y-1);
        recurse(oldc,newc,x-1,y);
      }
      //else if top, bottom left and right:
      else if((matrix[x][y].getY() == 0)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x,y+1);
        recurse(oldc,newc,x-1,y);
        recurse(oldc,newc,x+1,y);
      }
      //else if left, top bottom and right:
      else if((matrix[x][y].getX() == 0)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x,y-1);
        recurse(oldc,newc,x,y+1);
        recurse(oldc,newc,x+1,y);
      }
      //else if right, left bottom and top:
      else if((matrix[x][y].getX() == 900)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x-1,y);
        recurse(oldc,newc,x,y+1);
        recurse(oldc,newc,x,y-1);
      }
      //else if bottom, top left and right:
      else if((matrix[x][y].getY() == 900)){
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x,y-1);
        recurse(oldc,newc,x-1,y);
        recurse(oldc,newc,x+1,y);
      }
      //else its in the middle so check all:
      else{
        matrix[x][y].setCheck(1);
        recurse(oldc,newc,x,y-1);
        recurse(oldc,newc,x,y+1);
        recurse(oldc,newc,x-1,y);
        recurse(oldc,newc,x+1,y);
      }
    }
    
  }
  
}

//------------------------------------------------------------------ class Square:

class Square{
  
  int c1,c2,c3;
  float x,y;
  int c; //1-5 for red-purple
  int check;
  
  Square(float xx, float yy, int cc1, int cc2, int cc3, int cc){
    x=xx;
    y=yy;
    c1=cc1;
    c2=cc2;
    c3=cc3;
    c = cc;
    check = 0;
  }
  
  void display(){
    fill(c1,c2,c3);
    stroke(0);
    rect(x,y,100,100);
  }
  
  int getColor(){
    return c;
  }
  
  void setColor(int newc){
    c = newc;
    switch(newc){
      case 1: c1 = s1c1; c2 = s1c2; c3 = s1c3; break;
      case 2: c1 = s2c1; c2 = s2c2; c3 = s2c3; break;
      case 3: c1 = s3c1; c2 = s3c2; c3 = s3c3; break;
      case 4: c1 = s4c1; c2 = s4c2; c3 = s4c3; break;
      case 5: c1 = s5c1; c2 = s5c2; c3 = s5c3; break;
    }
    
  }
  
  float getX(){
    return x;
  }
  
  float getY(){
    return y;
  }
  
  int getCheck(){
    return check;
  }
  
  void setCheck(int c){
    check = c;
  }
}

//------------------------------------------------------------------ class Button:

class Button{
  
  float x,y;
  int c1,c2,c3;
  
  Button(float xx, float yy, int cc1, int cc2, int cc3){
    x=xx;
    y=yy;
    c1=cc1;
    c2=cc2;
    c3=cc3;
  }
  
  void display(){
    fill(c1,c2,c3);
    stroke(255);
    rect(x,y,104,150);
  }
  
}
